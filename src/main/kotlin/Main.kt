import java.util.Scanner
/**
*   @author Daniel Reinosa Luque
*   @version 1.1 31/05/2023
 */

var userList = mutableMapOf<String, String>("Jordi" to "qwertY123", "Jose" to "123456Jose", "David" to "DaWeD4B3sT")

fun checkPassword(password: String): Boolean {
    var lowerCase = false
    var upperCase = false
    var digit = false
    var symbol = false
    if (password.length < 8) return false
    for (character in password) {
        if (character in 'a'..'z') lowerCase = true
        if (character in 'A'..'Z') upperCase = true
        if (character in '0'..'9') digit = true
        if (character in '!'..'/' || character in ':'..'@' || character in '['..'`' || character in '{'..'~') symbol = true
    }
    return lowerCase && upperCase && digit && symbol
}

fun checkUsername(username: String): Boolean {
    return !userList.contains(username)
}

fun addUser(username: String, password: String) {
    userList.replace(username, password)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Welcome to ITB!!")
    println("Do you want to register a user? (yes/no)")
    while (scanner.next().uppercase() == "yes") {
        println("Insert a username:")
        val username = scanner.next()
        println("Insert a password:")
        val password = scanner.next()
        if (checkUsername(username) && checkPassword(password)) {
            addUser(username, password)
            println("User added to system")
        } else println("Username is repeated or password is not correct")
        println("Do you want to register another user? (yes/no)")
    }
}